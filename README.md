# MyTasks

MyTasks é um aplicativo de TODO List. Faça sua lista de tarefas e não perca mais nenhuma tarefa importante!

Android + Kotlin

## Introdução 

Este aplicativo foi desenvolvido para ser entregue como projeto final para o Bootcamp Santander Mobile, em parceria com DIO - Digital Innovation One. 
Trata-se de um aplicativo de lista de tarefas, constituindo basicamente um CRUD, onde o usuário do aplicativo poderá criar uma tarefa, listar suas tarefas criadas, modificar qualquer tarefa e também indicar que aquela tarefa foi concluída ou não. 

## Tecnologias e padrões usados

O aplicativo foi desenvolvido inteiramente em Kotlin para a plataforma Android (nativo). 

- Material Design 
- Fragments e FragmentManager
- RecyclerView e Custom Adapters
- MVVM (foi utilizado também o padrão Repository, ViewModel e LiveData)
- ROOM
- Coroutines
- Injeção básica de dependência via construtor
- ViewBinding

## Telas da aplicação
![1](/uploads/3b61a5407f33410192eaf5c51b906142/1.png)
![5](/uploads/8220aa253f643bfe0a094779f8a4b590/5.png)
![4](/uploads/cbc97b08a0b9db9c4d5fb3b14ffd36e6/4.png)
![3](/uploads/e254da0c53fc48abdcd08f91d982f9f8/3.png)
![6](/uploads/64782785e517f00b4e84af856061f7b9/6.png)


## Trabalhos futuros
A fim de melhorar a aplicação e a experiência do usuário com a mesma, abaixo encontram-se algumas melhorias a serem realizadas: 

- Melhorias da parte visual do aplicativo (UI/UX)
- Otimizações de código
- Implementar a funcionalidade de share, onde o usuário poderá compartilhar os dados de suas tarefas. 
- Implementar alarmes para lembrar o usuário sobre suas tarefas. 












